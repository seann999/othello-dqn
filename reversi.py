import pygame, sys, traceback, time, math, itertools

pygame.init()
bg = 0,0,0
#maximum amount of frames allowed per second
fps = 30
#set this to false to not render
render = False
#does the game run by itself or wait for a run command?
autoRun = False

if __name__ == "__main__":
    autoRun = True
    render = True

reward_scale = 1.0

#state enum
class State:
    Empty, White, Black = range(3)

    @staticmethod
    def toString(state):
        if state == State.White:
            return "White"
        if state == State.Black:
            return "Black"
        if state == State.Empty:
            return "Empty"

#board class
class Board:
    def __init__(self, idx, white = None, black = None):
        self.length = idx
        self.turn = State.White
        self.gameFinished = False
        self.coord = [[State.Empty for x in range(idx)] for y in range(idx)]
        if render:
            self.whiteImg = white
            self.blackImg = black
            self.white = white.get_rect()
            self.black = black.get_rect()
            self.constX = self.white.right + 8.8
            self.constY = self.white.bottom + 8.7
            self.pos = [[self.white.move([self.constX * y  + 13, self.constY * x  + 10]) for x in range(idx)] for y in range(idx)]

    # use this to print out the board onto the console.
    #0: blank
    #1: white
    #2: black
    def printData(self):
        for i in range(self.length):
            for j in range(self.length):
                print self.coord[j][i],
            print""
    
    def printPos(self):
        if not render:
            return
        for i in range(self.length):
            print ""
            for j in range(self.length):
                print "[" + str(self.pos[i][j].x) + ", " + str(self.pos[i][j].y) + "]",
        print ""
        
    #returns true if player has a valid move.
    def canPlace(self, player):
        for i in range(0, 8):
            for j in range(0, 8):
                if self.coord[i][j] != State.Empty:
                    continue
                if self.place(i, j, player, True) != 0:
                    return True
        return False

    #switches turns. calls this function again if the player has no valid moves.
    #returns True if game is continuing, returns False if game cannot be progressed any furthur
    def switchTurn(self):
        nextNextTurn = State.Black        
        
        if self.turn == State.White:
            self.turn = State.Black
            nextNextTurn = State.White 
        else:
            self.turn = State.White
            nextNextTurn = State.Black
        if not self.canPlace(self.turn):
            print State.toString(self.turn), "has no valid moves.", State.toString(nextNextTurn), "will move again."

            if not self.canPlace(nextNextTurn):
                self.printData()
                print("Neither player can make a move. Game is over.")
                return False
            else:
                self.switchTurn()
                
        return True
    
    #changes the state of the cell of the given coordinate
    #returns 0 if placement is invalid, -1 if game is over, number of pieces flipped if otherwise
    def place(self, x, y, state, check = False, initial = False):
        if initial:
            self.coord[x][y] = state
            return 0
        flipped = 0
        if self.coord[x][y] != State.Empty:
            return 0
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0:
                    continue
                xi = x + i
                yj = y + j
                if xi < 0 or xi > 7 or yj < 0 or yj > 7:
                    continue
                if self.coord[xi][yj] == state or self.coord[xi][yj] == State.Empty:
                    continue
                #print x + i, y + j, "Opponent"
                #counts the amount of pieces that are going ot be flipped
                xik = 0
                yjk = 0
                flipList = []
                for k in range(1, 8):
                    xik = x + (i * k)
                    yjk = y + (j * k)
                    if xik > 7 or xik < 0 or yjk > 7 or yjk < 0:
                        break
                    if self.coord[xik][yjk] == State.Empty:
                        break
                    #print "Checking: ", xik, yjk
                    if self.coord[xik][yjk] == state:
                        #print k-1, "pieces"
                        #print flipList
                        for a in flipList:
                            if  not check:
                                self.coord[a[0]][a[1]] = state
                        flipped += k-1
                        flipList = []
                        break;
                    flipList.append([xik, yjk])
        if flipped > 0 and not check:
            self.coord[x][y] = self.turn
            gameOver = False
            
            if self.checkVictory(self.turn) == None: # make sure game is going to continue and stop game otherwise
                gameOver = not self.switchTurn()
            
            if gameOver:
                self.gameFinished = True
                print "Game Finished: White:", self.getScore(State.White) , "Black:", self.getScore(State.Black)
                return -1
                                    
        return flipped
    def updateScreen(self):
        for i in range(self.length):
            for j in range(self.length):
                if self.coord[i][j] == State.Empty:
                    continue
                elif self.coord[i][j] == State.White:
                    screen.blit(self.whiteImg, self.pos[i][j])
                else:
                    screen.blit(self.blackImg, self.pos[i][j])
    # reset board
    def resetBoard(self):
        for i in range(self.length):
            for j in range(self.length):
                self.coord[i][j] == State.Empty
        self.place(3, 3, State.White, False, True)
        self.place(4, 3, State.Black, False, True)
        self.place(3, 4, State.Black, False, True)
        self.place(4, 4, State.White, False, True)
        self.gameFinished = False
                
    # checks victory condition for player. returns 1 if victorious, -1 if loss, None if game is not over yet, 0 if draw
    def checkVictory(self, player):
        if not self.gameFinished:
            return None
        
        white = 0
        black = 0
        for i in range(0, 8):
            for j in range(0, 8):
                if self.coord[i][j] == State.White:
                    white+=1
                else:
                    black+=1
        if white == black:
            return 0.5 * reward_scale
    
        if white > black:
            if player == State.White:
                return 1.0 * reward_scale
            return 0.0 * reward_scale
        if player == State.White:
            return 0.0 * reward_scale
        return 1.0 * reward_scale
    def getScore(self, player):
        score = 0
        for i in range(0, 8):
            for j in range(0,8):
                if self.coord[i][j] == player:
                    score += 1
        return score
            
screen = None
    
def runGame(screen, aiFile):
    useAI = False    
    
    if aiFile is not None:
        useAI = True
        import dqn
        ai = dqn.OthelloDQN(None, aiFile)
    
    global run, board, start, end
    while run:
        try:                
            if render:
                if useAI and board.turn == State.White and not board.gameFinished:
                    moves = ai.play(board.coord)
                     # first in list is *best move*
                    re = 0
                    index = 0
                    while re == 0:
                        coords = moves[index]
                        re = board.place(coords[0], coords[1], board.turn)
                        index += 1
                
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        run = False
                    if board.gameFinished:
                        break
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        pygame.mouse.get_rel()
                    if event.type == pygame.MOUSEBUTTONUP:
                        dif = pygame.mouse.get_rel()
                        if math.fabs(dif[0]) < 10 and math.fabs(dif[1]) < 10:
                            pos = pygame.mouse.get_pos()
                            x = int((pos[0] - 13)/board.constX)
                            y = int((pos[1] - 10)/board.constY)
                            if x < 8 and x >= 0 and y < 8 and y >= 0:
                                board.place(x, y, board.turn)
                                                            
                #FPS Control
                end = time.time()
                if (end - start) * fps >= 1:
                    
                    #Redraws Screen once per frame
                    screen.fill(bg)
                    screen.blit(boardImg, boardD)
                    board.updateScreen()
                    pygame.display.flip()
                    start = end
                    #cFps += 1
                    #if end - startEX >= 1:
                    #    print cFps
                    #    cFps = 0
                    #    startEX = end
            
        except Exception as e:
            traceback.print_exc()
            run = False
            
if autoRun or __name__ == "__main__": 
    #board asset upload and screen size initilization
    
    if render:
        
        boardImg = pygame.image.load("board.jpg")
        boardD = boardImg.get_rect()
        size = [boardD.right, boardD.bottom]
        
        screen = pygame.display.set_mode(size)
    
        #pieces asset upload
        whiteImg = pygame.image.load("white.png")
        blackImg = pygame.image.load("black.png")
    
    
    #main program
    run = True
    if autoRun:
        board = Board(8, whiteImg, blackImg)
    else:
        board = Board(8)
    board.resetBoard()
    start = time.time()
    end = time.time()
    
    #cFps = 0
    #startEX = start

    render = True
    runGame(screen, "saves/model-20000.ckpt")

    #terminate program
    pygame.quit()
