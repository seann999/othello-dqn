# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 00:39:40 2016

@author: sean
"""

import tensorflow as tf
import numpy as np
import math

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.01)
    return tf.Variable(initial)
    
def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)
    
def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding="SAME")
    
def max_pool_2x2(x):
    # strides -> stride size per dimension -> (batches(X), width(2), height(2), channels(X))
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")

b_w = 8 # board width
b_h = 8 # board height
gamma = 0.99
lr = 1e-5
batch_size = 16
initial_epsilon = 0.2
epsilon = initial_epsilon

class OthelloDQN:
    def __init__(self, save_path, load_path=None):
        inputs = b_h * b_w * 3
        actions = b_h * b_w
        
        self.save_path = save_path
        self.sess = tf.InteractiveSession()

        self.board_placeholder = tf.placeholder("float", shape=[None, b_h, b_w, 3])
        self.target_q_values = tf.placeholder("float", shape=[None, actions])
        
        #x_image = tf.reshape(self.board_placeholder, [batch_size, b_h, b_w, 3]) # batch size, height, width, channels
        
        conv_sizes = [[8, 32], [4, 32]]
        all_convs = []
        
        for conv_size in conv_sizes:
            filter_size = conv_size[0]
            n_maps = conv_size[1]
            W_conv1 = weight_variable([filter_size, filter_size, 3, n_maps]) # filter height, filter width, in channels, out channels
            b_conv1 = bias_variable([n_maps])
            h_conv1 = tf.nn.relu(conv2d(self.board_placeholder, W_conv1) + b_conv1)
            h_pool1 = max_pool_2x2(h_conv1) # 128 4x4 patches
            all_convs.append(h_pool1)
        
        convs_tensor = tf.concat(1, all_convs)
        #convs_tensor = tf.squeeze(convs_tensor, squeeze_dims=[0])
        shape = convs_tensor.get_shape()

        size = shape[1].value * shape[2].value * shape[3].value

        h_pool1_flat = tf.reshape(convs_tensor,  tf.pack([tf.shape(convs_tensor)[0], size]))
        
        W_fc1 = weight_variable([size, 64])
        b_fc1 = bias_variable([64])
        h_fc1 = tf.nn.relu(tf.matmul(h_pool1_flat, W_fc1) + b_fc1)
        
        #keep_prob = tf.placeholder("float")
        #h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
        
        W_fc2 = weight_variable([64, actions])
        b_fc2 = bias_variable([actions])
        self.q_values = tf.matmul(h_fc1, W_fc2) + b_fc2 # output q values for each action
        
        self.quad_loss = tf.reduce_mean(tf.square(self.target_q_values - self.q_values))
        self.train_step = tf.train.AdamOptimizer(lr).minimize(self.quad_loss)        
        
        
        summary_op = tf.merge_all_summaries()
        
        self.saver = tf.train.Saver()
        
        if load_path is not None:
            self.load(load_path)
        else:
            self.sess.run(tf.initialize_all_variables())
        
    def get_coords(self, a_index):
        return [a_index % b_w, int(math.floor(a_index / b_w))]
        
    def load(self, path):
        self.saver.restore(self.sess, path)
        print("loaded parameter file: " + path)
        
    def save(self, name):
        self.saver.save(self.sess, self.save_path + name + ".ckpt")
        
    def to_onehot(self, board):
        b = np.asarray(board.coord)
        b = convert_to_onehot_matrix(b)
        return b
        
    def printCoords(self, coords):
        s = ""
        for i in range(b_h):
            for j in range(b_w):
                s += str(coords[j][i]) + " "
            s += "\n"
            
        return s
        
    def printMat(self, coords):
        for i in range(b_h):
            for j in range(b_w):
                print coords[j][i],
            print ""
    
    def train(self, start_iter = 0, end_iter = 1000, oneTurn=True, use_board=None):
        debug_print = False
        experience = []
        max_experience = 256
        summary_writer = tf.train.SummaryWriter("summary", graph_def=self.sess.graph_def)
        winRate = 0
        
        for e in xrange(start_iter + 1, end_iter + 1):
            print("episode: " + str(e))

            qSum = 0
            qDiff = 0
            qN = 0      
            rSum = 0
            A = None
            
            if use_board == None:
                board_object = reversi.Board(8)
            else:
                board_object = use_board
            board_object.resetBoard()
            updated_board = None
            first_turn = True
            gameFinished = False

            while not gameFinished:
               currentTurn = board_object.turn
               r = board_object.checkVictory(reversi.State.White)
               
               if first_turn and currentTurn == reversi.State.White:
                   board = np.asarray(board_object.coord)
                   board_clean = self.printCoords(board_object.coord)
                   board = convert_to_onehot_matrix(board)
                   first_turn = False
               elif (currentTurn == reversi.State.White) or (r is not None):
                   updated_board = self.to_onehot(board_object)
                   updated_board_clean = self.printCoords(board_object.coord)
                   
                   endGame = False
                
                   if r is None: # game is continuing
                       r = 0
                   else:
                       endGame = True
                    
                   rSum += r
                   experience.append([board, A, r, updated_board, endGame])
                 
                   """print("============")
                   print(board_clean)
                   print(">>> " + str(self.get_coords(A)))
                   print(updated_board_clean)
                   print("///")
                   self.printMat(board)
                   print("---------")
                   self.printMat(updated_board)
                   print("---------")
                   self.printMat(np.equal(updated_board, board))
                   print(">>>>> " + str(r))"""
                   
                   board = updated_board
                   board_clean = updated_board_clean     
                   
                   if endGame:
                       gameFinished = True
                       
               if not gameFinished:
                   q = self.sess.run(self.q_values, feed_dict={self.board_placeholder : board.reshape(1, b_h, b_w, 3)})[0] # calculate q values from S and all As
                   q_sorted = np.argsort(q) # sort by highest priority to lowest
                   top = [q.shape[0] - 1] # list instead of number because Python 2 scoping mess
                   move_made = False
    
                   #moveQ = 0
                   
    
                   if debug_print:
                       print("new move iteration; turn: " + reversi.State.toString(currentTurn))
                   
                   while not move_made:
                       def epsilon_greedy(q, epsilon):
                           if top[0] < 0:
                               print(str(currentTurn))
                               board_object.printData()
                              
                               raise ValueError("negative action index")
                               return -1
                           
                           u = np.random.uniform()
                           if u < epsilon:
                               return q_sorted[np.random.randint(top[0] + 1)]
                           else:
                               re = q_sorted[top[0]]
                               top[0] = top[0] - 1
                               return re
                       
                       if currentTurn == reversi.State.White:
                           a_index = epsilon_greedy(q_sorted, epsilon)
                           A = a_index
                       else:
                           a_index = epsilon_greedy(q_sorted, 1.0)
                       
                       coords = self.get_coords(a_index) # x, y coordinate
                       # take action a, get r and s_prime (updated board) -> move_made = True
                       
                       validMove = board_object.place(coords[0], coords[1], currentTurn)
                       
                       if validMove == 0:
                           # get invalid move response -> move_made remains False, top -= 1
                           if debug_print:
                               print(str(top[0]) + " invalid move " + str(coords) + " attempted by " + str(currentTurn))
                       elif validMove == -1:
                           move_made = True # technically incorrect, but game is over so
                           if debug_print:
                               print("jumping out of loop (ending game) ...")
                           continue
                       else:
                           move_made = True
                           
                           if currentTurn == reversi.State.Black:
                               continue                       
                           
                           if debug_print:
                               print("move " + str(coords) + " made by " + str(reversi.State.toString(currentTurn)))
                               
                           #moveQ = q[a_index]
                           qSum += q[a_index]
                           qN += 1
                           
                           if debug_print:
                               board_object.printData()
                               print("---------")
                 
               if currentTurn == reversi.State.Black:
                   continue
               
               if len(experience) > max_experience:
                   del experience[np.random.randint(len(experience))]
            
               if len(experience) > batch_size:
                   sample = [experience[i] for i in np.random.randint(len(experience), size=batch_size)]
                   
                   sample_board = []
                   sample_new_q = []
                   
                   for s in sample:
                       q = self.sess.run(self.q_values, feed_dict={self.board_placeholder : s[0].reshape(1, b_h, b_w, 3)})[0]
                       
                       q_prime = self.sess.run(self.q_values, feed_dict={self.board_placeholder : s[3].reshape(1, b_h, b_w, 3)})[0]
                       q_prime_max = np.max(q_prime)
                        
                       new_q = np.copy(q)
                        
                       if s[4]:
                           new_q[s[1]] = s[2]
                       else:
                           new_q[s[1]] = s[2] + gamma * q_prime_max# update q value
                       
                       sample_board.append(s[0])
                       sample_new_q.append(new_q)
                       
                       
                   sample_board = np.asarray(sample_board)
                   sample_new_q = np.asarray(sample_new_q)
                   
                   _, loss, q_values_fetched = self.sess.run([self.train_step, self.quad_loss, self.q_values], \
                   feed_dict={self.board_placeholder : sample_board, self.target_q_values : sample_new_q}) # update q values
                   
                   
                   #print(sample_new_q)
                   #print(q_values_fetched)
                   #print("--")
                    
                   qDiff += loss
                
        
            if debug_print:
                print("concluded game.")
            avgQ = qSum / qN
            avgDiffQ = qDiff / qN
        
            if qN > 0:
                print("average Q: %f" % avgQ)
            else:
                print("qN is zero!")
                
            victory = board_object.checkVictory(reversi.State.White)
            print("final reward: %f" % (victory))
            
            if e % 10 == 0:
                summary = tf.Summary(value=[tf.Summary.Value(tag="average q", simple_value=avgQ)])
                summary_writer.add_summary(summary, global_step=e)
            
                summary = tf.Summary(value=[tf.Summary.Value(tag="error q", simple_value=avgDiffQ)])
                summary_writer.add_summary(summary, global_step=e)
                             
                qSum = 0
                qDiff = 0
                qN = 0

            print("epsilon %f" % (epsilon))            
            print("========")
            
            #global epsilon
            #if e != start_iter and e % 1000 == 0:
            #    epsilon -= 0.02
            #    epsilon = max(0.01, epsilon)
            #epsilon = max((1.0 - e / 5000.0) * initial_epsilon, 0.0)
            
            if victory >= reversi.reward_scale:
                winRate += 1.0
            
            if e != 0 and e % 100 == 0:
                rate = winRate / 100.0
                summary = tf.Summary(value=[tf.Summary.Value(tag="win rate", \
                simple_value=rate)])
                summary_writer.add_summary(summary, global_step=e)
                winRate = 0
                
                summary = tf.Summary(value=[tf.Summary.Value(tag="error q", simple_value=rSum/10.0)])
                summary_writer.add_summary(summary, global_step=e)
                rSum = 0
                
            if e % 1000 == 0: 
                print("saving model...")                   
                self.save("model-" + str(e))
                #tf.scalar_summary("reward", r)
            
        print("DONE")
    
    def play(self, board_coords):
        board = np.asarray(board_coords)
        board = convert_to_onehot_matrix(board)
        q = self.sess.run(self.q_values, feed_dict={self.board_placeholder : board.reshape(1, b_h, b_w, 3)})[0]
        q_sorted = np.argsort(q) # sort by highest priority to lowest
        coords = map(self.get_coords, q_sorted)
        return coords
        
def convert_to_onehot_matrix(mat):
    re = np.zeros((b_h * b_w, 3))
    mat = np.reshape(mat, (b_h * b_w))
    
    re[range(b_h * b_w), mat] = 1
    
    re = np.reshape(re, (b_h, b_w, 3))
    
    return re
    
def generate_phony_board():
    return convert_to_onehot_matrix(np.random.randint(3, size=(b_h, b_w)))
        
if __name__ == "__main__":
    agent1 = OthelloDQN("savesA/")
    agent2 = OthelloDQN("savesB/")
    import reversi
    print("start training...")
    #agent1.train(10000, 20000)
    #board = reversi.board